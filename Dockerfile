FROM ruby:2.3.3

RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends apt-utils
RUN apt-get install build-essential vim python-dev python-pip -y
RUN pip install awscli --upgrade --user
RUN echo 'export PATH=/root/.local/bin:$PATH' >> ~/.bashrc

RUN echo "syntax on\nset number" > ~/.vimrc

COPY . /usr/src/app

WORKDIR /usr/src/app

CMD bash
