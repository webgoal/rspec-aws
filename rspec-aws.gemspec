Gem::Specification.new do |s|
  s.name        = 'rspec-aws'
  s.version     = '0.0.1'
  s.date        = '2018-12-29'
  s.summary     = "Run Rspec test on EC2 instance!"
  s.description = "Run all your suite test at Amazon via parallel tests"
  s.authors     = ["Herbertt Bamonde"]
  s.email       = 'bamonde@webgoal.com.br'
  s.files       = Dir.glob("{bin,lib}/**/*") + %w(README.md)
  s.executables = ["raws"]
  s.homepage    = 'http://rubygems.org/gems/rspec-aws'
  s.license     = 'MIT'
  s.post_install_message = <<-EOF

  Read the steps below:

  1. You'll need AWS Cli to use this Gem.

    Check if you've installed `aws cli`.

     If not, access the link below and follow the instructions

    - https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html

    If you already have installed, follow next step.

  2. You'll need a AWS profile configurated.

    Profiles are stored on file ~/.aws/credentials

    Check the profile was created, if not, run the command below to configure:

    aws configure --profile <profile-name>

EOF
end
