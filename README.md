# Rspec Aws

Running Rspec tests on EC2 Instances in a parallel way.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

For this Gem works you need to install some dependencies in your system.

* [Pip](https://pypi.org/project/pip/) - The tool for installing Python packages
* [AWS Cli](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) - Command Line Interface for Amazon AWS
* [AWS Profile](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html#cli-quick-configuration-multi-profiles) - Create AWS Cli profile to use further
* [rsync](https://help.ubuntu.com/community/rsync) - Syncronize files

Create a instance on EC2 with keypair that you'll use later. Get all the params specified below and create env vars.

## For use gem

Add to your Gemfile:

```
gem 'rspec-aws', bitbucket: 'webgoal/rspec-aws', branch: 'master'
```

Create a those ENV VARS with respective values:

```
RSPEC_AWS_PROFILE=profile_name
RSPEC_AWS_INSTANCE_ID=instance_id
RSPEC_AWS_PUBLIC_PATH=instance_public_ip
RSPEC_AWS_USER=instance_user
RSPEC_AWS_PROJECT_FOLDER=path_for_project_inside_instance
```

Create a aws profile under ~/.aws folder.

You'll need:

* AWS_ACCESS_KEY_ID
* AWS_SECRET_ACCESS_KEY

Configure profile:

Assuming that you profile will labeled as `default`, so, use it on `RSPEC_AWS_PROFILE` envvar.
You can change it too, but don't forget to change at echo lines below.

- Docker

Add these lines in Dockerfile to fill the files
```
$ mkdir -p ~/.aws
$ echo $'[default]\naws_access_key_id = AWS_ACCESS_KEY_ID\naws_secret_access_key = AWS_SECRET_ACCESS_KEY' > ~/.aws/credentials
$ echo $'[default]\nregion = eu-west-2\noutput = json' > ~/.aws/config
```

- Via AWS Cli

```
$ aws configure
```
Follow the instructions

### Execute

To run, execute with bundle to load the gem:

```
$ bundle exec raws
```

You'll see the options:

* setup: for build the build containers
* reset: remove all containers
* rspec: run all tests

## For develop this Gem

### Docker

To build:

```
$ docker-compose build
```

To enter in container:

```
$ docker-compose run --rm app bash
```

### Run gem locally

First:

```
$ gem build rspec-aws.gemspec
```

Second:

```
$ gem install ./rspec-aws-0.0.0.gem
```

Or just run command below to make things happen:

```
$ ./exec.sh
```

Third:

```
$ irb

>> require 'rspec-aws'
```

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
