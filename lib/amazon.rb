# frozen_string_literal: true

require 'json'
require 'singleton'
require 'yaml'
require 'load_params'

# Amazon commands
class Amazon
  include Singleton
  include LoadParams

  def initialize
    super
    exit! unless @config.any?
  end

  def stopped?
    ec2_status = describe
    ec2_status['Reservations'][0]['Instances'][0]['State']['Name'] == 'stopped'
  end

  def started? # rubocop:disable Metrics/AbcSize
    return false if stopped? || describe['Reservations'][0]['Instances'][0]['State']['Name'] != 'running'
    ec2_status = describe_status
    ec2_status['InstanceStatuses'].any? &&
      ec2_status['InstanceStatuses'][0]['InstanceState']['Name'] == 'running' &&
      ec2_status['InstanceStatuses'][0]['SystemStatus']['Status'] == 'ok' &&
      ec2_status['InstanceStatuses'][0]['InstanceStatus']['Status'] == 'ok'
  end

  def start
    JSON.parse(`aws ec2 start-instances --instance-ids #{@config[:instance_id]} --profile #{@config[:profile]}`)
  end

  def stop
    JSON.parse(`aws ec2 stop-instances --instance-ids #{@config[:instance_id]} --profile #{@config[:profile]}`)
  end

  def describe
    JSON.parse `aws ec2 describe-instances --instance-ids #{@config[:instance_id]} --profile #{@config[:profile]}`
  end

  def describe_status
    JSON.parse(`aws ec2 describe-instance-status --instance-ids #{@config[:instance_id]} --profile #{@config[:profile]} --include-all-instances`)
  end

  def status
    ec2_status = describe_status
    "Status #{ec2_status['InstanceStatuses'][0]['InstanceState']['Name']} - System #{ec2_status['InstanceStatuses'][0]['SystemStatus']['Status']} - Instance #{ec2_status['InstanceStatuses'][0]['InstanceStatus']['Status']}".white
  end

  def sync
    puts 'Synchronizing files'
    puts `#{ssh} 'sudo rm -rf #{@config[:project_folder]}/log && sudo rm -rf #{@config[:project_folder]}/tmp && mkdir -p #{@config[:project_folder]}'`
    puts `rsync -Crazvhp --delete --exclude=coverage/ --exclude=node_modules/ --exclude=log/ --exclude=tmp/ --exclude=.git/ -e "ssh -i ~/.ssh/rspec-instance-lumiar.pem" .  #{@config[:user]}@#{@config[:public_path]}:#{@config[:project_folder]}`
    puts 'Done'
  end

  def rspec
    puts `#{ssh} 'cd #{@config[:project_folder]} && docker-compose run --rm app bash -c "
            bundle install && rails db:drop db:create db:migrate"'`
    puts `#{ssh} 'cd #{@config[:project_folder]} && docker-compose run --rm app bash -c "
            rails parallel:drop &&
            rails parallel:create &&
            rails parallel:prepare &&
            COVERAGE=true rails parallel:spec[#{number_of_cores}]"'`
  end

  def number_of_cores
    `#{ssh} 'cat /proc/cpuinfo | grep processor | wc -l'`.to_i - 1
  end

  def environment_variable
    puts `#{ssh} 'export IMAGE_TAG=rspec_amazon'`
  end

  def language
    puts `#{ssh} 'touch $HOME/.i18n && echo "LANG=en_US.utf-8" >> $HOME/.i18n && echo "LC_ALL=en_US.utf-8" >> $HOME/.i18n'`
  end

  def docker
    puts `#{ssh} '
          sudo yum update -y &&
          sudo yum install -y docker htop &&
          sudo service docker start &&
          sudo usermod -a -G docker ec2-user'`
  end

  def docker_compose
    puts `#{ssh} '
          sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) | sudo tee /usr/local/bin/docker-compose > /dev/null &&
          sudo chmod +x /usr/local/bin/docker-compose &&
          sudo service docker start &&
          sudo chkconfig docker on'`
  end

  def build_containers
    puts `#{ssh} 'cd #{@config[:project_folder]} && docker-compose build'`
  end

  def reset
    puts 'Reseting instance...'.yellow
    puts `#{ssh} '
      rm $HOME/.i18n &&
      docker stop $(docker ps -aq) &&
      docker rm $(docker ps -aq) &&
      docker rmi $(docker images -aq) &&
      docker system prune -af &&
      sudo chkconfig docker off &&
      sudo service docker stop &&
      sudo yum remove -y docker htop &&
      sudo rm /usr/local/bin/docker-compose'`
    puts 'Done'.green
  end

  def public_path
    ec2_status = describe
    ec2_status['Reservations'][0]['Instances'][0]['PublicDnsName']
  end

  def download
    puts 'Downloading reports'.green
    print `#{scp}:#{@config[:project_folder]}/coverage/* ./coverage && echo '...Coverage files downloaded'`.magenta
    print `#{scp}:"#{@config[:project_folder]}/tmp/failing_specs.log #{@config[:project_folder]}/tmp/summary_specs.log" ./tmp/. && echo '...Failing specs files downloaded'`.magenta
    puts 'All downloads are done'.green
    puts 'You can shutdown instance executing command below or waiting auto shutdown:'.cyan
    puts '  ./amazon.sh stop:'.cyan
  end

  def set_shutdown
    puts 'Set up Shutdown'
    puts `#{ssh} 'sudo shutdown -P +90 &> /dev/null &'`
  end

  def cancel_shutdown
    puts 'Canceling Shutdown'
    puts `#{ssh} 'sudo shutdown -c'`
  end

  def build_finish(finished)
    if finished
      puts `#{ssh} 'rm #{@config[:project_folder]}/../spec_running.txt'`
    else
      puts `#{ssh} 'touch #{@config[:project_folder]}/../spec_running.txt'`
    end
  end

  def spec_running?
    `#{ssh} 'ls #{@config[:project_folder]}/../spec_running.txt 2> /dev/null | wc -l'`.to_i > 0
  end

  def ssh_command
    puts 'Entering on instance...'.green
    puts
    puts 'Command: '.white
    puts ssh.to_s.cyan
    puts
    system ssh
  end

  private

  def scp
    "scp -ri #{local_ssh_key_path} #{@config[:user]}@#{@config[:public_path]}"
  end

  def ssh
    "ssh -i #{local_ssh_key_path} #{@config[:user]}@#{@config[:public_path]}"
  end

  def local_ssh_key_path
    '~/.ssh/rspec-instance-lumiar.pem'
  end
end
