# frozen_string_literal: true

# Load all env vars to hash
module LoadParams
  def initialize
    raise StandardError unless (ENV.keys & %w[RSPEC_AWS_PROFILE RSPEC_AWS_INSTANCE_ID RSPEC_AWS_PUBLIC_PATH RSPEC_AWS_USER RSPEC_AWS_PROJECT_FOLDER]).count == 5
    @config = {
      profile: ENV['RSPEC_AWS_PROFILE'],
      instance_id: ENV['RSPEC_AWS_INSTANCE_ID'],
      public_path: ENV['RSPEC_AWS_PUBLIC_PATH'],
      user: ENV['RSPEC_AWS_USER'],
      project_folder: ENV['RSPEC_AWS_PROJECT_FOLDER']
    }
  rescue StandardError
    puts "\nSome of env vars are not defined. Check then and try again.\n\nRSPEC_AWS_PROFILE, RSPEC_AWS_INSTANCE_ID, RSPEC_AWS_PUBLIC_PATH, RSPEC_AWS_USER and RSPEC_AWS_PROJECT_FOLDER\n\n"
    @config = {}
  end
end
