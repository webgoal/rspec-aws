require 'amazon'
require 'string'

# Parse commands from shell to Amazon
class RspecAws
  def self.ssh
    Amazon.instance.ssh_command
  end

  def self.status
    puts "\nInstance is #{Amazon.instance.status}\n\n".white
  end

  def self.download
    Amazon.instance.cancel_shutdown
    Amazon.instance.download
    Amazon.instance.set_shutdown
  end

  def self.run
    instance_boot
    spec_run
  end

  def self.reset
    Amazon.instance.reset
  end

  def self.setup
    Amazon.instance.environment_variable
    Amazon.instance.language
    Amazon.instance.docker
    Amazon.instance.docker_compose
    Amazon.instance.sync
    Amazon.instance.build_containers
  end

  def self.start
    if Amazon.instance.stopped?
      puts 'Instance is turned off'.yellow
      puts 'Starting'.white
      Amazon.instance.start
      sleep 3 until Amazon.instance.started?
      puts 'Started'.green
    else
      puts Amazon.instance.status.white
    end
  end

  def self.stop
    if Amazon.instance.stopped?
      puts 'Instance is turned off'.yellow
    else
      puts 'Stoping...'.red
      Amazon.instance.stop
      sleep 3 until Amazon.instance.stopped?
      puts 'Stopped'.green
    end
  end

  def self.instance_boot
    if Amazon.instance.stopped?
      puts "AWS EC2 Instance is turned off\nStarting instance..."
      Amazon.instance.start
      sleep 3 until Amazon.instance.started?
    elsif Amazon.instance.started?
      puts 'AWS EC2 Intance is started and ready!'.green
    else
      puts 'Waiting AWS EC2 Instance to be ready...'.yellow
      sleep 3 until Amazon.instance.started?
    end
  end

  def self.spec_run
    return if spec_running?
    begin
      Amazon.instance.build_finish(false)
      Amazon.instance.cancel_shutdown
      Amazon.instance.sync
      Amazon.instance.rspec
      Amazon.instance.download
      Amazon.instance.set_shutdown
    ensure
      Amazon.instance.build_finish(true)
    end
  end

  def self.spec_running?
    return false unless Amazon.instance.spec_running?
    puts 'There is a Rspec running...'.white
    puts 'Try again later!'.white
    true
  end
end
